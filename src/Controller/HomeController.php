<?php

namespace App\Controller;

use App\Entity\News;
use App\Entity\Page;
use App\Services\ConfigService;
use App\Services\FakePathService;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;

class HomeController extends AbstractController {

    /** @var ConfigService */
    private $cfg;

    /** @var FakePathService */
    private $fps;

    public function __construct(ConfigService $cfg, FakePathService $fps)
    {
        $this->cfg = $cfg;
        $this->fps = $fps;
    }

    public function index() {
        $mainPage = $this->cfg->getConfig('homepage', 'login')->getValue();
        return $this->redirectToRoute('display_page', ['page' => $mainPage]);
    }

    public function displayPage(Request $rq, string $page) {
        $p = $this->fps->getPath($page);
        $pageElement = $this->getDoctrine()->getRepository(Page::class)->findOneBySlug($p->getPage());
        return $this->render('page.html.twig', ['page' => $pageElement, 'request' => $rq, 'fakepath'=> $p]);
    }

    public function displayOneNews(string $slug) {
        $news = $this->getDoctrine()->getRepository(News::class)->findOneBy(['slug' => $slug]);
        return $this->render('news.html.twig', ['news' => $news]);
    }

}