import {applyRemove} from "./base";
import '../../css/admin/news.edit.scss';

import Quill from 'quill';

applyRemove('Vous allez supprimer la news "%NAME%" !', 'news');

let contentFR = document.getElementById('news_content_fr');
let contentEN = document.getElementById('news_content_en');

// If we are on the edit page
if (contentFR || contentEN) {

    let quill_content_fr = new Quill('#quillbox_content_fr', {
        modules: {
            toolbar: '#quillbox_toolbar_fr'
        },
        placeholder: 'Écrire une news...',
        theme: 'snow'
    });

    try {
        quill_content_fr.setContents(JSON.parse(contentFR.value));
    } catch (e) {}

    let quill_content_en = new Quill('#quillbox_content_en', {
        modules: {
            toolbar: '#quillbox_toolbar_en'
        },
        placeholder: 'Write a news...',
        theme: 'snow'
    });

    try {
        quill_content_en.setContents(JSON.parse(contentEN.value));
    } catch (e) {}

    let form = document.getElementById('news-edit-form');
    form.onsubmit = () => {
        contentFR.value = JSON.stringify(quill_content_fr.getContents());
        contentEN.value = JSON.stringify(quill_content_en.getContents());

        return true;
    }
}
