// Admin Base JS file //
import '../../css/admin/base.scss';
import 'vex-js/sass/vex.sass';
import 'vex-js/sass/vex-theme-os.sass';

import '../../images/delete.png';
import '../../images/edit.png';
import '../../images/view.png';

import * as vex from 'vex-js';
import * as vexDialog from 'vex-dialog';

vex.registerPlugin(vexDialog);
vex.defaultOptions.className = 'vex-theme-os';

function displayRemove(message, path, name, id) {
        vex.dialog.confirm({
            unsafeMessage:  message.replace("%NAME%", name),
            callback: function (value) {
                if (value) {
                    fetch('/admin/' + path + '/delete/' + id, { method: "DELETE" })
                        .then((resp) => {
                            if (resp.status === 410) {
                                location.reload();
                                return null;
                            } else {
                                return resp.text();
                            }
                        })
                        .then((text) => {
                            if (text)
                                vex.dialog.alert("Erreur: " + text);
                        });
                }
            }
        })
}

export function applyRemove(message, path) {
    let delButtons = document.getElementsByClassName("delete-button");

    for (let i = 0; i < delButtons.length; i++) {
        delButtons[ i ].onclick = function () {
            displayRemove(message, path, delButtons[ i ].getAttribute('data-name'), delButtons[ i ].getAttribute('data-id'));
        };
    }
}
