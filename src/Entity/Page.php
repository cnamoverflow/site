<?php
/**
 * @author Nathan JANCZEWSKI <nathan@janczewski.fr>
 */

namespace App\Entity;

use App\Entity\Blocks\Block;
use App\Traits\IDTrait;
use App\Traits\SlugTrait;
use DateTime;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\PageRepository")
 * @ORM\Table(name="page")
 */
class Page
{
    use IDTrait, SlugTrait;

    /**
     * @ORM\Column(type="string", length=30)
     */
    protected $title_fr;

    /**
     * @ORM\Column(type="string", length=30, nullable=true)
     */
    protected $title_en;

    /**
     * @ORM\ManyToMany(targetEntity="App\Entity\Blocks\Block", cascade={"persist"})
     * @ORM\JoinTable(name="page_block",
     *      joinColumns={@ORM\JoinColumn(name="page_id", referencedColumnName="id", onDelete="CASCADE")},
     *      inverseJoinColumns={@ORM\JoinColumn(name="block_id", referencedColumnName="id", unique=true)}
     *      )
     * @ORM\OrderBy({"order" = "ASC"})
     */
    protected $blocks;

    /**
     * @ORM\Column(type="datetime")
     */
    protected $creationDate;

    /**
     * @ORM\Column(type="boolean", options={"default": false})
     */
    protected $superadminOnly;

    public function getTitleFr(): ?string
    {
        return $this->title_fr;
    }

    public function setTitleFr(string $title_fr): Page
    {
        $this->title_fr = $title_fr;
        return $this;
    }

    public function getTitleEn(): ?string
    {
        return $this->title_en;
    }

    public function setTitleEn(string $title_en): Page
    {
        $this->title_en = $title_en;
        return $this;
    }

    public function getCreationDate(): ?DateTime
    {
        return $this->creationDate;
    }

    public function setCreationDate(DateTime $creationDate): ?Page
    {
        $this->creationDate = $creationDate;
        return $this;
    }

    public function addBlock(Block $block): Page
    {
        $this->blocks[] = $block;
        return $this;
    }

    public function isSuperadminOnly(): bool
    {
        return $this->superadminOnly;
    }

    public function setSuperadminOnly(bool $sao): Page
    {
        $this->superadminOnly = $sao;
        return $this;
    }

    /**
     * @return ArrayCollection
     */
    public function getBlocks()
    {
        return $this->blocks;
    }

    public function __construct()
    {
        $this->superadminOnly = false;
    }

}