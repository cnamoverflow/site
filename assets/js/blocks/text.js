import Quill from 'quill';

let blocks = document.getElementsByClassName("block_text");
for (let b of blocks) {
    let quill = new Quill(b);
    quill.setContents(JSON.parse(b.innerText));
    quill.enable(false);
}