# Dockerless setup

1. Set the php pool
2. Set the nginx config
3. Create DB user / DB
    su postgres
    createuser cnamoverflow
    exit
    su
    passwd cnamoverflow
    exit
    su postgres
    createdb -O cnamoverflow cnamoverflowdb
    exit

4. composer install
5. yarn install
6. yarn run encore production
7. export $(grep -v "^#" env)     # Source the env file for the current ssh session
8. bin/console doctrine:schema:create
9. bin/console doctrine:fixtures:load # TODO: export default values to have a simple home page
