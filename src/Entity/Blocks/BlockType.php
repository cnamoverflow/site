<?php

namespace App\Entity\Blocks;

class BlockType
{

    const Text     = "TEXT";
    const Title    = "TITLE";
    const News     = "NEWS";
    const BandMenu = "BANDMENU";

}
