<?php

namespace App\Repository;

use App\Entity\Page;
use Doctrine\ORM\EntityRepository;

class PageRepository extends EntityRepository
{

    public function findOneBySlug(string $slug): ?Page {
        $qb = $this->createQueryBuilder('p')
            ->select('p')
            ->where('p.slug = :slug')
            ->setParameter('slug', $slug)
            ->setMaxResults(1);
        return $qb->getQuery()->getOneOrNullResult();
    }
}
