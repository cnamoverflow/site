<?php
/**
 * @author Nathan JANCZEWSKI <nathan@janczewski.fr>
 */

namespace App\Entity\Blocks;

use App\Services\FakePath;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity()
 */
class BlockText extends Block
{

    /**
     * @ORM\Column(name="text", type="text", nullable=true)
     */
    protected $text;

    public function getText(): ?string
    {
        return $this->text;
    }

    public function setText(string $text): BlockText
    {
        $this->text = $text;
        return $this;
    }

    public function getBlockType(): string
    {
        return BlockType::Text;
    }

    public function getBlockTypeName(): string
    {
        return "Texte";
    }

    public function getParameters(FakePath $fp, EntityManagerInterface $emi): array
    {
        return [
            'text' => $this->text
        ];
    }

}