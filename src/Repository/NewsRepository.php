<?php
/**
 * @author Nathan JANCZEWSKI <nathan@janczewski.fr>
 */

namespace App\Repository;


use Doctrine\ORM\EntityRepository;

class NewsRepository extends EntityRepository
{

    public function getCount(): int {
        return $this->createQueryBuilder('n')->select('COUNT(n)')->getQuery()->getSingleResult()[1];
    }

    public function getNewsPaginated(int $offset, int $maxResult): array {
        $qb = $this->createQueryBuilder('n')
            ->select('n')
            ->orderBy('n.creationDate', 'DESC')
            ->setFirstResult($offset)
            ->setMaxResults($maxResult);

        return $qb->getQuery()->getResult();
    }
}