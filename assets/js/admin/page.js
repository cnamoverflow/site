// Admin Base JS file //
import '../../css/admin/page.scss';
import 'vex-js/sass/vex.sass';
import 'vex-js/sass/vex-theme-os.sass';

import * as vex            from 'vex-js';
import { applyRemove }     from "./base";
import { showBlockEditor } from "./block_editor";

vex.defaultOptions.className = 'vex-theme-os';

applyRemove('Vous allez supprimer la page "%NAME%".<br /> Attention, si la page est présente dans un block ou sur un menu, elle sera supprimée de ces derniers aussi.', 'page');

let dropZone = document.getElementById("BlockDropZone");
let pickZone = document.getElementById("BlockPickZone");

function applyBlockRemove(page, element) {
    element.onclick = function () {
        vex.dialog.confirm({
            unsafeMessage: "Êtes-vous sur de vouloir supprimer le block %NAME% de la page ?".replace('%NAME%', element.getAttribute('data-blockname')),
            callback     : function (value) {
                if (value) {
                    fetch('/admin/page/' + page + "/block/" + element.getAttribute("data-blockid"), { method: 'DELETE' })
                        .then((resp) => {
                            if (resp.status === 410) {
                                element.remove();
                            } else {
                                vex.dialog.alert('Une erreur est survenue ! (' + resp.status + ')');
                                console.log("Err: " + resp.status);
                            }
                            return null;
                        });
                }
            }
        });
    };
}

function applyBlockEdit(page, element) {
    fetch('/admin/page/' + page + '/block/' + element.getAttribute("data-blockid"))
        .then((resp) => {
            return resp.json();
        })
        .then((json) => {
            showBlockEditor(json, (data) => {
                let headers = new Headers();
                headers.append("Content-Type", "application/json");

                fetch('/admin/page/' + page + '/block/' + element.getAttribute('data-blockid'), { method:"PUT", headers, body: JSON.stringify(data)})
                    .then((resp) => {
                        return resp.json();
                    })
                    .then((json) => {
                        console.log(json);
                    });
            });
        });
}


if (dropZone && pickZone) {
    let pageID = location.href;
    pageID     = pageID.substring(pageID.lastIndexOf("/") + 1, pageID.length);

    dropZone.ondragover = function (ev) {
        ev.preventDefault();
    };

    dropZone.ondrop = function (ev) {
        ev.preventDefault();
        let data      = ev.dataTransfer.getData("text/html");
        let elt       = document.getElementById(data).cloneNode(true);
        let blocktype = elt.id.substring(elt.id.indexOf("-") + 1, elt.id.length);

        let URL_ICON_EDIT   = document.getElementById("URL_ICON_EDIT").value;
        let URL_ICON_REMOVE = document.getElementById("URL_ICON_REMOVE").value;

        elt.removeAttribute("id");
        elt.removeAttribute("draggable");
        dropZone.appendChild(elt);

        fetch("/admin/page/" + pageID + "/block/" + blocktype, { method: 'POST' })
            .then((resp) => {
                if (resp.status !== 201) {
                    elt.remove();
                    return "{}";
                }
                return resp.json();
            })
            .then((json) => {
                json = JSON.parse(json); // Wtf ??
                if (json["id"]) {
                    elt.setAttribute("data-blockid", json["id"]);

                    /* Creating elements */
                    let outterDiv = document.createElement('div');

                    let button1 = document.createElement('img');
                    button1.alt = "editer";
                    button1.src = URL_ICON_EDIT;
                    button1.classList.add("clickable");
                    button1.onclick = () => applyBlockEdit(pageID, elt);
                    outterDiv.appendChild(button1);

                    let button2 = document.createElement('img');
                    button2.alt = "supprimer";
                    button2.src = URL_ICON_REMOVE;
                    button2.classList.add("clickable");
                    button2.onclick = () => applyBlockRemove(pageID, elt);
                    outterDiv.appendChild(button2);

                    elt.appendChild(outterDiv);
                }
            });
    };

    for (let e of pickZone.getElementsByTagName("li")) {
        e.ondragstart = function (ev) {
            ev.dataTransfer.setData("text/html", ev.target.id);
        };
    }

    for (let e of dropZone.getElementsByTagName("li")) {
        let div        = e.getElementsByTagName("div")[0].getElementsByTagName("img");
        div[0].onclick = () => applyBlockEdit(pageID, e);
        div[1].onclick = () => applyBlockRemove(pageID, e);
    }
}
