<?php
/**
 * @author Nathan JANCZEWSKI <nathan@janczewski.fr>
 */

namespace App\Model;

use App\Entity\Config;

class SuperConfigModel
{

    /** @var Config */
    private $footer;

    /** @var Config */
    private $dirpub;

    public function getFooter(): ?Config {
        return $this->footer;
    }

    public function getDirPub(): ?Config {
        return $this->dirpub;
    }

    public function setFooter(?Config $footer): SuperConfigModel {
        $this->footer = $footer;
        return $this;
    }

    public function setDirPub(?Config $dirpub): SuperConfigModel {
        $this->dirpub = $dirpub;
        return $this;
    }

}