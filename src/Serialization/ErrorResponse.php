<?php
/**
 * @author Nathan JANCZEWSKI <nathan@janczewski.fr>
 */

namespace App\Serialization;


use Symfony\Component\HttpFoundation\JsonResponse;

class ErrorResponse extends JsonResponse
{

    public function __construct(string $msg, int $status = 400, array $headers = array())
    {
        parent::__construct(json_encode(["message" => $msg]), $status, $headers, true);
    }

}