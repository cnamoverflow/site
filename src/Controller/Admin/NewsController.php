<?php
/**
 * @author Nathan JANCZEWSKI <nathan@janczewski.fr>
 */

namespace App\Controller\Admin;


use App\Entity\News;
use App\Forms\NewsType;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class NewsController extends AbstractController
{
    /**
     * @Route("/admin/news", name="admin_news_list")
     */
    public function admin_news_list() {
        $news = $this->getDoctrine()->getRepository(News::class)->findBy([], ['creationDate'=>'DESC']);
        return $this->render('admin/news/list.html.twig', ['news' => $news]);
    }

   /**
     * @Route("/admin/news/new", name="admin_news_new")
     */
    public function admin_news_new(Request $rq) {
        $news = new News();
        $form = $this->createForm(NewsType::class, $news);

        $form->handleRequest($rq);

        if ($form->isSubmitted() && $form->isValid()) {
            $news->setCreationDate(new \DateTime());
            $news->setWriter($this->getUser());
            $news->updateGeneratedSlug($news->getTitleEN());

            $em = $this->getDoctrine()->getManager();
            $em->persist($news);
            $em->flush();

            return $this->redirectToRoute('admin_news_list');
        }

        return $this->render('admin/news/edit.html.twig', ['form' => $form->createView()]);
    }

    /**
     * @Route("/admin/news/edit/{id}", name="admin_news_edit")
     */
    public function admin_news_edit(Request $rq, News $news) {
        $form = $this->createForm(NewsType::class, $news);

        $form->handleRequest($rq);

        if ($form->isSubmitted() && $form->isValid()) {
            $news->setCreationDate(new \DateTime()); // Modification date ?
            $news->updateGeneratedSlug($news->getTitleEN());

            //$news->setWriter($this->getUser());

            $em = $this->getDoctrine()->getManager();
            $em->persist($news);
            $em->flush();

            return $this->redirectToRoute('admin_news_list');
        }

        return $this->render('admin/news/edit.html.twig', ['form' => $form->createView()]);
    }

    /**
     * @Route("/admin/news/delete/{id}", name="admin_news_delete", methods={"DELETE"})
     */
    public function delete(Request $rq, string $id) {
        $news = $this->getDoctrine()->getRepository(News::class)->findOneBy(['id' => $id]);

        if ($news !== null) {
            $this->getDoctrine()->getManager()->remove($news);
            $this->getDoctrine()->getManager()->flush();
            return new Response("News deleted!", 410);
        } else {
            return new Response("News not found!", 404);
        }
    }


}