<?php
/**
 * @author Nathan JANCZEWSKI <nathan@janczewski.fr>
 */

namespace App\Services;


use App\Entity\Blocks\Block;
use App\Entity\Blocks\BlockText;
use App\Entity\Blocks\BlockTitle;
use App\Entity\Blocks\BlockType;
use Doctrine\ORM\EntityManagerInterface;
use Exception;

class BlockBuilderService
{

    /** @var EntityManagerInterface */
    private $emi;

    public function __construct(EntityManagerInterface $emi)
    {
        $this->emi = $emi;
    }

    public function buildBlock(string $blocktype, int $order)
    {
        $block = null;

        switch ($blocktype) {
            case BlockType::Text:
                $block = new BlockText();
                $block->setOrder($order);
                break;
            case BlockType::Title:
                $block = new BlockTitle();
                $block->setOrder($order);
                break;
            default:
                break;
        }

        return $block;
    }

    /**
     * @param $idBlock
     * @param $data
     * @throws Exception
     * @return block id
     */
    public function editBlock($idBlock, $data)
    {
        $block = $this->emi->getRepository(Block::class)->findOneBy(['id' => $idBlock]);

        if ($block === null)
            throw new Exception('This block does not exists!');

        if (array_key_exists('order', $data) && gettype($data['order']) !== 'integer')
            throw new Exception('The order must be an integer');

        $order = $data['order'] ?? $block->getOrder();
        $block->setOrder($order);

        switch ($block->getBlockType()) {
            case BlockType::Text:
                if ($data['content'])
                    $block->setText($data["content"]);
                break;
            case BlockType::Title:
                if ($data['content'])
                    $block->setTitle($data['content']);
                break;
            default:
                throw new Exception('The block type is invalid');
        }

        $this->emi->persist($block);
        $this->emi->flush();

        return $block->getID();

    }

}