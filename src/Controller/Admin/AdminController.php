<?php
/**
 * @author Nathan JANCZEWSKI <nathan@janczewski.fr>
 */

namespace App\Controller\Admin;

use App\Forms\MainConfigType;
use App\Model\MainConfigModel;
use App\Services\ConfigService;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;

class AdminController extends AbstractController {

    /** @var ConfigService */
    private $cs;

    /** @var EntityManagerInterface */
    private $emi;

    public function __construct(ConfigService $cfg, EntityManagerInterface $emi)
    {
        $this->cs = $cfg;
        $this->emi = $emi;
    }

    /**
     * @Route("/admin", name="admin_index")
     */
    public function index() {
        return $this->redirectToRoute('admin_news_list');
    }

    /**
     * @Route("/admin/config", name="admin_config")
     */
    public function config(Request $rq) {

        $confModel = new MainConfigModel();
        $confModel->setCompanyName($this->cs->getConfig('company_name', 'MyCompany'));
        $confModel->setSiteURL($this->cs->getConfig('site_url', ''));

        $form = $this->createForm(MainConfigType::class, $confModel);
        $form->handleRequest($rq);

        if ($form->isSubmitted() && $form->isValid()) {
            $confModel = $form->getData();

            foreach ($confModel->getConfig() as $value) {
                $this->emi->persist($value);
                $this->emi->flush();
            }
        }

        return $this->render("admin/config.html.twig", ["form" => $form->createView()]);
    }

}

