<?php
/**
 * @author Nathan JANCZEWSKI <nathan@janczewski.fr>
 */

namespace App\DataFixtures;


use App\Entity\Config;
use Doctrine\Common\Persistence\ObjectManager;

class ConfigFixtures
{
    /** @var ObjectManager */
    private static $om;

    static function generate(ObjectManager $om)
    {
        self::$om = $om;

        self::addConfig("footer", "true");
        self::addConfig("homepage", "home");
    }

    private static function addConfig(string $key, string $value)
    {

        $cfg = (new Config())->setKey($key)->setValue($value);
        self::$om->persist($cfg);
    }

}