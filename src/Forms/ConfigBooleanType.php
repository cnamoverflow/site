<?php
/**
 * @author Nathan JANCZEWSKI <nathan@janczewski.fr>
 */

namespace App\Forms;

use App\Entity\Config;
use App\Entity\News;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\CallbackTransformer;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class ConfigBooleanType extends AbstractType
{

    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->add(
            $builder->create('value', CheckboxType::class, [
                'label' => $options['text'],
                'required' => false
            ])
                ->addModelTransformer(new CallbackTransformer(
                    function ($booleanValue) {
                        return $booleanValue == "true" ? true : false;
                    },
                    function ($stringValue) {
                        return $stringValue ? "true" : "false";
                    }
                ))
        );
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'text' => '',
            'data_class' => Config::class
        ]);
    }

}