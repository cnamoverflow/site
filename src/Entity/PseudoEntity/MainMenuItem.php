<?php
/**
 * @author Nathan JANCZEWSKI <nathan@janczewski.fr>
 */

namespace App\Entity\PseudoEntity;

use App\Entity\Page;
use App\Repository\MainMenuItemRepository;
use App\Traits\IDTrait;
use App\Traits\OrderTrait;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity
 * @ORM\Table(name="MainMenu")
 */
class MainMenuItem
{
    use IDTrait, OrderTrait;

    /**
     * @ORM\Column(type="string", length=10)
     */
    protected $text_fr;

    /**
     * @ORM\Column(type="string", length=10)
     */
    protected $text_en;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Page")
     * @ORM\JoinColumn(name="dest", referencedColumnName="id", onDelete="CASCADE")
     */
    protected $dest;

    public function setTextFr(string $txt): MainMenuItem
    {
        $this->text_fr = $txt;
        return $this;
    }

    public function setTextEn(string $txt): MainMenuItem
    {
        $this->text_en = $txt;
        return $this;
    }

    public function setDestination(Page $page): MainMenuItem
    {
        $this->dest = $page;
        return $this;
    }

    public function getTextFr(): ?string
    {
        return $this->text_fr;
    }

    public function getTextEn(): ?string
    {
        return $this->text_en;
    }

    public function getDestination(): ?Page
    {
        return $this->dest;
    }

    public function getOrder(): ?int
    {
        return $this->order;
    }

    public function setOrder(int $order): MainMenuItem
    {
        $this->order = $order;
        return $this;
    }

}