<?php
/**
 * @author Nathan JANCZEWSKI <nathan@janczewski.fr>
 */

namespace App\DataFixtures;


use App\Entity\User;
use Doctrine\Common\Persistence\ObjectManager;
use FOS\UserBundle\Model\UserManagerInterface;

class UserFixtures
{

    /** @var UserManagerInterface $fosuser */
    static $fosuser;

    static function generate(ObjectManager $om, UserManagerInterface $fosuser) {
        self::$fosuser = $fosuser;

        $om->persist(self::createUser('fAdmin', 'lAdmin', 'admin', ['ROLE_ADMIN']));
        $om->persist(self::createUser('Nathan', 'JANCZEWSKI', 'oxodao', ['ROLE_SUPER_ADMIN']));
        $om->persist(self::createUser('fSAdmin', 'lSAdmin', 'superadmin', ['ROLE_SUPER_ADMIN']));

        for ($i = 1; $i < 10; $i++) {
            $om->persist(self::createUser('fUser', 'lUser', 'user'.$i, ['ROLE_USER']));
        }


    }

    private static function createUser(string $fName, string $lName, string $username, array $roles) {
        /** @var User $user */
        $user = self::$fosuser->createUser();
        $user->setUsername($username);
        $user->setFirstName($fName)->setLastName($lName);
        $user->setEmail($username . '@cnam-overflow.dev');
        $user->setPlainPassword($username);
        $user->setRoles($roles);
        $user->setEnabled(true);

        return $user;
    }

}
