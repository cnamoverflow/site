<?php

namespace App\DataFixtures;

use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\Persistence\ObjectManager;
use FOS\UserBundle\Doctrine\UserManager;
use FOS\UserBundle\Model\UserManagerInterface;

class AppFixtures extends Fixture
{
    /** @var UserManager $fosuser */
    private $fosuser;

    public function __construct(UserManagerInterface $fosuser)
    {
        $this->fosuser = $fosuser;
    }

    public function load(ObjectManager $manager)
    {
        ConfigFixtures::generate($manager);
        UserFixtures::generate($manager, $this->fosuser);
        PageFixtures::generate($manager);
        NewsFixtures::generate($manager);

        $manager->flush();
    }
}
