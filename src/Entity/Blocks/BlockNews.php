<?php
/**
 * @author Nathan JANCZEWSKI <nathan@janczewski.fr>
 */

namespace App\Entity\Blocks;

use App\Entity\News;
use App\Services\FakePath;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\Mapping as ORM;
use Pagerfanta\Adapter\DoctrineORMAdapter;

/**
 * @ORM\Entity()
 */
class BlockNews extends Block
{
    /**
     * @ORM\Column(name="newsPerPage", type="integer")
     */
    private $newsPerPage;

    /**
     * @ORM\Column(name="fullsizeNews", type="boolean")
     */
    private $fullsizeNews;

    public function getNewsPerPage(): int {
        return $this->newsPerPage;
    }

    public function setNewsPerPage(int $npp): BlockNews {
        $this->newsPerPage = $npp;
        return $this;
    }

    public function isFullsizeNews(): bool {
        return $this->fullsizeNews;
    }

    public function setFullsizeNews(bool $fs): BlockNews {
        $this->fullsizeNews = $fs;
        return $this;
    }

    public function getBlockType(): string
    {
        return BlockType::News;
    }

    public function getBlockTypeName(): string
    {
        return "News";
    }

    public function getParameters(FakePath $fp, EntityManagerInterface $emi): array
    {
        $page = intval($fp->getArg("news", 0));
        $page--;

        if ($page < 0) {
            $page = 0;
        }

        $displayedNews = $emi->getRepository(News::class)->findBy([], [/*'creationDate' => 'DESC'*/], $this->newsPerPage, $page * $this->newsPerPage);
//        $displayedNews = $emi->getRepository(News::class)->getNewsPaginated($page * $this->getNewsPerPage(), $this->$this->getNewsPerPage());

        $amtPages = ceil(($emi->getRepository(News::class)->getCount()) / $this->newsPerPage);
        return [
            'news' => $displayedNews,
            'fakepath' => $fp,
            'amtPages' => $amtPages
        ];
    }

}