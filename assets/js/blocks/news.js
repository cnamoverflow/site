import Quill from 'quill';

let contents = document.getElementsByClassName('news_content');
for (let i = 0; i < contents.length; i++) {
    let tx = JSON.parse(contents[i].innerText);
    let quill = new Quill(contents[i]);
    quill.setContents(tx);
    quill.disable();
}