<?php
/**
 * @author Nathan JANCZEWSKI <nathan@janczewski.fr>
 */

namespace App\Forms;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Security\Core\Authorization\AuthorizationChecker;
use Symfony\Component\Security\Core\Authorization\AuthorizationCheckerInterface;

class PageType extends AbstractType
{

    /** @var AuthorizationCheckerInterface */
    private $auth;

    public function __construct(AuthorizationCheckerInterface $authorizationChecker)
    {
        $this->auth = $authorizationChecker;
    }

    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('title_fr')
            ->add('title_en');

        if ($this->auth->isGranted("ROLE_SUPER_ADMIN")) {
            $builder->add('superadminOnly', CheckboxType::class, ['label' => "Super Admin uniquement?", 'required' => false]);
        }

        $builder->add('save', SubmitType::class);
    }

}