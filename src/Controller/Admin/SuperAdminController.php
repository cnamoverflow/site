<?php
/**
 * @author Nathan JANCZEWSKI <nathan@janczewski.fr>
 */

namespace App\Controller\Admin;

use App\Forms\ConfigBooleanType;
use App\Forms\SuperConfigType;
use App\Model\SuperConfigModel;
use App\Services\ConfigService;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;

class SuperAdminController extends AbstractController {

    /** @var ConfigService */
    private $cs;

    /** @var EntityManagerInterface */
    private $emi;

    public function __construct(ConfigService $cfg, EntityManagerInterface $emi)
    {
        $this->cs = $cfg;
        $this->emi = $emi;
    }

    /**
     * @Route("/admin/super", name="super_admin_config")
     */
    public function index(Request $rq) {
        $hasFooter = $this->cs->getConfig('footer', 'false');
        $dirpub = $this->cs->getConfig('dirpub', '<DIRECTEUR DE PUBLICATION>');

        $model = (new SuperConfigModel())->setFooter($hasFooter)->setDirPub($dirpub);

        $form = $this->createForm(SuperConfigType::class, $model);
        $form->handleRequest($rq);

        if ($form->isSubmitted() && $form->isValid()) {
            /** @var SuperConfigModel $model */
            $model = $form->getData();

            $this->emi->persist($model->getFooter());
            $this->emi->persist($model->getDirPub());
            $this->emi->flush();
        }

        return $this->render('admin/superadmin.html.twig', ['form' => $form->createView()]);
    }

}

