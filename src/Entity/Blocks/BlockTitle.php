<?php
/**
 * @author Nathan JANCZEWSKI <nathan@janczewski.fr>
 */

namespace App\Entity\Blocks;

use App\Services\FakePath;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity
 */
class BlockTitle extends Block
{

    /**
     * @ORM\Column(type="string", length=20, nullable=true)
     */
    private $title;

    public function setTitle(string $title): BlockTitle
    {
        $this->title = $title;
        return $this;
    }

    public function getTitle(): ?string
    {
        return $this->title;
    }

    public function getBlockType(): string
    {
        return BlockType::Title;

    }

    public function getBlockTypeName(): string
    {
        return "Titre";
    }

    public function getParameters(FakePath $fp, EntityManagerInterface $emi): array
    {
        return [
            'text' => $this->title
        ];
    }

}