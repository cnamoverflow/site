<?php
/**
 * @author Nathan JANCZEWSKI <nathan@janczewski.fr>
 */

namespace App\Services;


use App\Entity\Config;
use Doctrine\ORM\EntityManagerInterface;

class ConfigService
{
    private $emi;

    public function __construct(EntityManagerInterface $emi)
    {
        $this->emi    = $emi;
    }

    public function getConfig(string $key, string $default)
    {
        $val = $this->emi->getRepository(Config::class)->findOneBy(['key' => $key]);

        if ($val === null) {
            $val = (new Config())->setKey($key)->setValue($default);
        }

        return $val;
    }
}