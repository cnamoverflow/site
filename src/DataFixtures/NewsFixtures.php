<?php
/**
 * @author Nathan JANCZEWSKI <nathan@janczewski.fr>
 */

namespace App\DataFixtures;


use App\Entity\Blocks\BlockNews;
use App\Entity\Blocks\BlockText;
use App\Entity\Blocks\BlockTitle;
use App\Entity\News;
use App\Entity\Page;
use App\Entity\PseudoEntity\MainMenuItem;
use App\Entity\User;
use Doctrine\Common\Persistence\ObjectManager;

class NewsFixtures
{

    static function generate(ObjectManager $om)
    {
        $user             = $om->getRepository(User::class)->findOneBy(['firstName' => 'Nathan', 'lastName' => 'JANCZEWSKI']);

        $text = '{"ops":[{"insert":"Qui cum venisset ob haec festinatis itineribus Antiochiam, praestrictis palatii ianuis, contempto Caesare, quem videri decuerat, ad praetorium cum pompa sollemni perrexit morbosque diu causatus nec regiam introiit nec processit in publicum, sed abditus multa in eius moliebatur exitium addens quaedam relationibus supervacua, quas subinde dimittebat ad principem.\n"}]}';
        $news = [
            self::createNews('Nouveau site web', 'New website', '{"ops":[{"insert":"Bienvenue sur le nouveau site de l\'équipe "},{"attributes":{"italic":true},"insert":"Overflow"},{"insert":", association du bureau des étudiants de la filiaire "},{"attributes":{"underline":true},"insert":"informatique"},{"insert":" du CNAM Reims \n"}]}', '{"ops":[{"insert":"Bienvenue sur le nouveau site de l\'équipe "},{"attributes":{"italic":true},"insert":"Overflow"},{"insert":", association du bureau des étudiants de la filiaire "},{"attributes":{"underline":true},"insert":"informatique"},{"insert":" du CNAM Reims \n"}]}', $user),
        ];

        for ($i = 0; $i < 50; $i++) {
            $news[] = self::createNews('News' . $i . 'fr', 'News' . $i . 'en', $text, $text, $user);
        }

        foreach ($news as $newsElt) {
            $om->persist($newsElt);
        }

        $om->flush();
    }

    private static function createNews(string $titleFR, string $titleEN, string $innerFR, string $innerEN, User $user): News
    {
        $news = new News();
        $news->setTitleFR($titleFR)->setTitleEN($titleEN);
        $news->setContentFR($innerFR)->setContentEN($innerEN);
        $news->setCreationDate(new \DateTime());
        $news->setWriter($user);
        $news->updateGeneratedSlug($titleFR);

        return $news;
    }

}
