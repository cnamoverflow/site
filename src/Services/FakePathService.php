<?php
/**
 * @author Nathan JANCZEWSKI <nathan@janczewski.fr>
 */

namespace App\Services;


class FakePathService
{

    public function getPath(string $path): FakePath {
        return new FakePath($path);
    }

}

class FakePath {

    /**
     * @var string
     */
    private $page;

    /**
     * @var array
     */
    private $args;

    public function __construct(string $path)
    {
        $page = explode('/', $path);
        $pathArgs = [];

        for ($i = 1; $i < count($page)-1; $i += 2)
        {
            $pathArgs[$page[$i]] = $page[$i+1];
        }

        $this->page = $page[0];
        $this->args = $pathArgs;
    }

    public function getPage(): string {
        return $this->page;
    }

    public function setPage(string $page): FakePath {
        $this->page = $page;
        return $this;
    }

    public function getArg(string $key, string $default = null): ?string {
        if (array_key_exists($key, $this->args)) {
            return $this->args[$key];
        }

        return $default;
    }

    public function setArg(string $key, string $value): FakePath {
        $this->args[$key] = $value;
        return $this;
    }

    public function buildURL() {
        return $this->buildURLWithTempArgs($this->args);
    }

    public function buildURLWithTempArgs(array $arr) {
        $str = '/' . $this->page;

        foreach ($arr as $k => $v) {
            $str .= '/' . $k . '/' . $v;
        }

        return $str;
    }

}