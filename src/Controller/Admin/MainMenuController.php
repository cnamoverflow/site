<?php
/**
 * @author Nathan JANCZEWSKI <nathan@janczewski.fr>
 */

namespace App\Controller\Admin;


use App\Entity\PseudoEntity\MainMenuItem;
use App\Forms\MainMenuType;
use App\Services\MainMenuFetcherService;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class MainMenuController extends AbstractController
{
    /** @var MainMenuFetcherService */
    private $mmf;

    public function __construct(MainMenuFetcherService $mmf)
    {
        $this->mmf = $mmf;
    }

    /**
     * @Route("/admin/mainmenu", name="admin_mainmenu_list")
     */
    public function index() {
        return $this->render('admin/mainmenu/list.html.twig', ['mmi' => $this->mmf->getMainMenu()]);
    }

    /**
     * @Route("/admin/mainmenu/add", name="admin_mainmenu_add")
     */
    public function add(Request $rq) {
        /** @var MainMenuItem $maxOrder */
        $maxOrder = $this->getDoctrine()->getRepository(MainMenuItem::class)->findOneBy([], ['order' => 'DESC']);
        $mmi = new MainMenuItem();

        if ($maxOrder !== null) {
            $mmi->setOrder($maxOrder->getOrder()+1);
        } else {
            $mmi->setOrder(1);
        }

        $form = $this->createForm(MainMenuType::class, $mmi);
        $form->handleRequest($rq);

        if ($form->isSubmitted() && $form->isValid()) {
            $doctrine = $this->getDoctrine()->getManager();
            $doctrine->persist($mmi);
            $doctrine->flush();

            return $this->redirectToRoute('admin_mainmenu_list');
        }

        return $this->render('admin/mainmenu/edit.html.twig', ['form' => $form->createView()]);
    }

     /**
     * @Route("/admin/mainmenu/edit/{id}", name="admin_mainmenu_edit")
     */
    public function edit(Request $rq, string $id) {
        $item = $this->getDoctrine()->getRepository(MainMenuItem::class)->findOneBy(['id' => $id]);

        if ($item === null) return $this->redirectToRoute('admin_mainmenu_list');

        $form = $this->createForm(MainMenuType::class, $item);
        $form->handleRequest($rq);

        if ($form->isSubmitted() && $form->isValid()) {
            $doctrine = $this->getDoctrine()->getManager();
            $doctrine->persist($item);
            $doctrine->flush();

            return $this->redirectToRoute('admin_mainmenu_list');
        }

        return $this->render('admin/mainmenu/edit.html.twig', ['form' => $form->createView()]);
    }

    /**
     * @Route("/admin/mainmenu/delete/{id}", name="admin_mainmenu_delete", methods={"DELETE"})
     */
    public function delete(Request $rq, string $id) {
        $item = $this->getDoctrine()->getRepository(MainMenuItem::class)->findOneBy(['id' => $id]);

        if ($item !== null) {
            $this->getDoctrine()->getManager()->remove($item);
            $this->getDoctrine()->getManager()->flush();
            return new Response("Menu item deleted ", 410);
        } else {
            return new Response("Menu item not found !", 404);
        }
    }
}