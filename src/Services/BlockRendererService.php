<?php
/**
 * @author Nathan JANCZEWSKI <nathan@janczewski.fr>
 */

namespace App\Services;

use App\Entity\Blocks\Block;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Templating\EngineInterface;

class BlockRendererService
{

    private $engine;
    private $emi;

    public function __construct(EngineInterface $engine, EntityManagerInterface $emi)
    {
        $this->engine = $engine;
        $this->emi    = $emi;
    }

    public function getHTML(Block $block, FakePath $fp): string
    {
        return $this->engine->render('blocks/block_' . strtolower($block->getBlockType()) . '.html.twig', $block->getParameters($fp, $this->emi));
    }

}