<?php
/**
 * @author Nathan JANCZEWSKI <nathan@janczewski.fr>
 */

namespace App\Entity\PseudoEntity;

use App\Entity\Page;
use App\Traits\IDTrait;
use App\Traits\OrderTrait;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity
 * @ORM\Table(name="bandmenu_item")
 */
class BandMenuItem
{

    use IDTrait, OrderTrait;

    /**
     * @ORM\Column(type="text", length=30)
     */
    private $text_fr;

    /**
     * @ORM\Column(type="text", length=30)
     */
    private $text_en;

    /**
     * @ORM\Column(type="text", length=20)
     */
    private $icon_name;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\PseudoEntity\BandMenu", inversedBy="menuItems")
     * @ORM\JoinColumn(name="menu_id", referencedColumnName="id")
     */
    private $menu;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Page")
     * @ORM\JoinColumn(name="dest", referencedColumnName="id", onDelete="CASCADE")
     */
    private $dest;

    public function getTextFr(): string
    {
        return $this->text_fr;
    }

    public function setTextFr($text_fr): BandMenuItem
    {
        $this->text_fr = $text_fr;
        return $this;
    }

    public function getTextEn(): string
    {
        return $this->text_en;
    }

    public function setTextEn($text_en): BandMenuItem
    {
        $this->text_en = $text_en;
        return $this;
    }

    public function getIconName(): string
    {
        return $this->icon_name;
    }

    public function setIconName($icon_name): BandMenuItem
    {
        $this->icon_name = $icon_name;
        return $this;
    }

    public function getMenu(): BandMenu
    {
        return $this->menu;
    }

    public function setMenu($menu): BandMenuItem
    {
        $this->menu = $menu;
        return $this;
    }

    public function getDestination(): Page
    {
        return $this->dest;
    }

    public function setDestination(Page $dest): BandMenuItem
    {
        $this->dest = $dest;
        return $this;
    }

}