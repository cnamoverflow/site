<?php

namespace App\Commands;

use App\Entity\Blocks\BlockNews;
use App\Entity\Config;
use App\Entity\News;
use App\Entity\Page;
use App\Entity\PseudoEntity\MainMenuItem;
use App\Entity\User;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Style\SymfonyStyle;

class InitializeCommand extends Command
{

    protected static $defaultName = "app:initialize";

    /** @var EntityManagerInterface */
    private $emi;

    public function __construct(EntityManagerInterface $emi)
    {
        parent::__construct(self::$defaultName);
        $this->emi = $emi;
    }

    protected function configure()
    {
        $this
            ->setDescription('Initialize the website')
            ->setHelp('This creates the homepage, the default config and such');
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $io = new SymfonyStyle($input, $output);

        $io->title("Initializing the website");

        $user = new User();
        $user->setUsername($io->ask("Username"));
        $user->setFirstName($io->ask("First name"));
        $user->setLastName($io->ask("Last name"));
        $user->setEmail($io->ask("Email"));
        $user->setPlainPassword($io->askHidden("Password"));
        $user->addRole("ROLE_ADMIN");
        $user->addRole("ROLE_SUPER_ADMIN");
        $user->setEnabled(true);
        $this->emi->persist($user);
        $io->writeln("User created");

        /** Creating the homepage **/
        $page = new Page();
        $page->setTitleFr('Accueil')->setTitleEn('Home');
        $page->setSlug('home');
        $page->setCreationDate(new \DateTime());

        $mmi = new MainMenuItem();
        $mmi->setOrder(0);
        $mmi->setTextEn("Home");
        $mmi->setTextFr("Accueil");
        $mmi->setDestination($page);
        $this->emi->persist($mmi);
        $this->emi->persist(new Config("homepage", "home"));
        $io->writeln("Home page created!");

        /** Adding a news block **/
        $newsBlock = new BlockNews();
        $newsBlock->setOrder(1);
        $newsBlock->setNewsPerPage(5);
        $newsBlock->setFullsizeNews(false);
        $page->addBlock($newsBlock);
        $io->writeln("News block added!");

        $this->emi->persist($page);

        /** Adding one news **/
        $news = new News();
        $title = "Nouveau site!";
        $news->setTitleFR($title)->setTitleEN("New website!");
        $news->setContentFR("{\"ops\":[{\"insert\":\"Ceci est votre nouveau site propulsé par le CMS de BDE-Overflow.\nConnectez vous avec votre compte pour commencer à ajouter du contenu.\n\"}]}");
        $news->setContentEN("{\"ops\":[{\"insert\":\"This is your new website powered by BDE-Overflow's CMS.\nLogin with your account to start adding content.\n\"}]}");
        $news->setCreationDate(new \DateTime());
        $news->setWriter($user);
        $news->updateGeneratedSlug($title);
        $this->emi->persist($news);
        $io->writeln("First news added!");

        /** Setting up the default config */
        $cfg = (new Config())->setKey('footer')->setValue('true');
        $this->emi->persist($cfg);
        $io->writeln("Default config created!");

        $this->emi->flush();
        $io->success("Website initialized!");

        $io->note("You can now login with the account you created.");
    }

}
