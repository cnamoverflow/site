<?php
/**
 * @author Nathan JANCZEWSKI <nathan@janczewski.fr>
 */

namespace App\Services;


use App\Entity\PseudoEntity\MainMenuItem;
use Doctrine\Common\Persistence\ObjectManager;

class MainMenuFetcherService
{

    private $om;

    public function __construct(ObjectManager $om)
    {
        $this->om = $om;
    }

    public function getMainMenu(): array
    {
        return $this->om->getRepository(MainMenuItem::class)->findBy([], ["order" => "ASC"]);
    }

}