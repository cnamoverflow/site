<?php
/**
 * @author Nathan JANCZEWSKI <nathan@janczewski.fr>
 */

namespace App\DataFixtures;


use App\Entity\Blocks\BlockBandMenu;
use App\Entity\Blocks\BlockNews;
use App\Entity\Blocks\BlockText;
use App\Entity\Blocks\BlockTitle;
use App\Entity\Page;
use App\Entity\PseudoEntity\BandMenu;
use App\Entity\PseudoEntity\BandMenuItem;
use App\Entity\PseudoEntity\MainMenuItem;
use Doctrine\Common\Persistence\ObjectManager;

class PageFixtures
{

    /** @var ObjectManager om */
    private static $om;

    static function generate(ObjectManager $om)
    {
        self::$om = $om;

        $members  = self::createPage("Membres", "Members");
        $projects = self::createPage("Projets", "Projects");
        $contact  = self::createPage("Contact", "Contact");
        $test     = self::createPage("test", "test");

        $home     = self::createHomePage([$members, $projects, $contact, $test]);

        $om->flush();

        self::createMainMenuItem($home, 0);
        self::createMainMenuItem($members, 0);
        self::createMainMenuItem($projects, 2);
        self::createMainMenuItem($contact, 3);

        $om->flush();
    }

    private static function createPage(string $pageNameFR, string $pageNameEN): Page
    {
        $page = new Page();
        $page->setTitleEn($pageNameEN)->setTitleFr($pageNameFR);
        $page->setCreationDate(new \DateTime());
        $page->updateGeneratedSlug($pageNameEN);

        $title = new BlockTitle();
        $title->setTitle("Test title");
        $title->setOrder(0);
        $page->addBlock($title);

        $blockText = new BlockText();
        $blockText->setText("BlockText pour la page " . $pageNameFR);
        $blockText->setOrder(1);

        $page->addBlock($blockText);

        self::$om->persist($page);

        return $page;
    }


    private static function createHomePage(array $pages): Page
    {
        $page = new Page();
        $page->setTitleFr('Accueil')->setTitleEn('Home');
        $page->setSlug('home');
        $page->setCreationDate(new \DateTime());

        $bandMenu = new BandMenu();
        $bandMenu->setMenuName("main_page_menu");
        $bandMenu->addMenuItem(self::buildMenuItem($bandMenu, "Informations", "Informations", "infos.png", 0, $pages[0]));
        $bandMenu->addMenuItem(self::buildMenuItem($bandMenu, "La formation", "Formation", "formation.png", 1, $pages[1]));
        $bandMenu->addMenuItem(self::buildMenuItem($bandMenu, "Nous rejoindre", "Join us", "rejoindre.png", 2, $pages[2]));
        $bandMenu->addMenuItem(self::buildMenuItem($bandMenu, "Devis projet", "Project quotation", "devis.png", 3, $pages[3]));

        $blockBandMenu = new BlockBandMenu();
        $blockBandMenu->setOrder(0);
        $blockBandMenu->setBandMenu($bandMenu);
        $page->addBlock($blockBandMenu);

        $newsBlock = new BlockNews();
        $newsBlock->setOrder(1);
        $newsBlock->setFullsizeNews(true)->setNewsPerPage(4);
        $page->addBlock($newsBlock);

        self::$om->persist($page);

        return $page;
    }

    private static function buildMenuItem(BandMenu $bm, string $txFr, string $txEn, string $iconName, int $order, Page $destination): BandMenuItem
    {
        $bmi = new BandMenuItem();
        $bmi->setTextFr($txFr)->setTextEn($txEn);
        $bmi->setIconName($iconName);
        $bmi->setOrder($order);
        $bmi->setMenu($bm);
        $bmi->setDestination($destination);

        return $bmi;
    }

    private static function createMainMenuItem(Page $page, int $order): MainMenuItem
    {
        $mmi = new MainMenuItem();
        $mmi->setTextEn($page->getTitleEn())->setTextFr($page->getTitleFr());
        $mmi->setOrder($order);
        $mmi->setDestination($page);

        self::$om->persist($mmi);
        return $mmi;
    }

}
