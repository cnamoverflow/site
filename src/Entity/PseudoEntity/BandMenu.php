<?php
/**
 * @author Nathan JANCZEWSKI <nathan@janczewski.fr>
 */

namespace App\Entity\PseudoEntity;


use App\Entity\Blocks\BlockBandMenu;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use App\Traits\IDTrait;

/**
 * @ORM\Entity
 * @ORM\Table(name="bandmenu")
 */
class BandMenu
{

    use IDTrait;

    /**
     * @ORM\Column(type="text", length=30)
     */
    protected $menuName;

    /**
     * @ORM\OneToOne(targetEntity="App\Entity\Blocks\BlockBandMenu", mappedBy="bandMenu", cascade={"persist", "remove"})
     */
    protected $block;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\PseudoEntity\BandMenuItem", mappedBy="menu", cascade={"persist", "remove"})
     * @var ArrayCollection
     */
    protected $menuItems;

    public function __construct()
    {
        $this->menuItems = new ArrayCollection();
    }

    public function getMenuName(): string
    {
        return $this->menuName;
    }

    public function setMenuName($menuName): BandMenu
    {
        $this->menuName = $menuName;
        return $this;
    }

    public function addMenuItem(BandMenuItem $menuItem): BandMenu
    {
        $this->menuItems->add($menuItem);
        return $this;
    }

    public function getMenuItems(): Collection
    {
        return $this->menuItems;
    }

    public function getBlock(): BlockBandMenu
    {
        return $this->block;
    }

    public function setBlock(BlockBandMenu $bbm): BandMenu
    {
        $this->block = $bbm;
        return $this;
    }

}