import {applyRemove} from "./base";

applyRemove('Vous allez supprimer l\'accès à la page "%NAME%" du menu.<br /> Attention, la page ne sera pas supprimée!', 'mainmenu');