<?php
/**
 * @author Nathan JANCZEWSKI <nathan@janczewski.fr>
 */

namespace App\Entity;
use App\Traits\IDTrait;
use App\Traits\SlugTrait;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping\OneToMany;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\NewsRepository")
 * @ORM\Table(name="News")
 */
class News
{
    use IDTrait, SlugTrait;

    /**
     * @ORM\Column(type="string", length=30)
     */
    protected $title_fr;

    /**
     * @ORM\Column(type="string", length=30)
     */
    protected $title_en;

    /**
     * @ORM\Column(type="text")
     */
    protected $content_fr;

    /**
     * @ORM\Column(type="text")
     */
    protected $content_en;

    /**
     * @ORM\Column(type="datetime")
     */
    protected $creationDate;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\User")
     * @ORM\JoinColumn(name="user_id", referencedColumnName="id")
     */
    protected $writer;

    public function getTitleFR()
    {
        return $this->title_fr;
    }

    public function setTitleFR($title_fr)
    {
        $this->title_fr = $title_fr;
        return $this;
    }

    public function getTitleEN()
    {
        return $this->title_en;
    }

    public function setTitleEN($title_en)
    {
        $this->title_en = $title_en;
        return $this;
    }

    public function getContentFR()
    {
        return $this->content_fr;
    }

    public function setContentFR($content_fr)
    {
        $this->content_fr = $content_fr;
        return $this;
    }

    public function getContentEN()
    {
        return $this->content_en;
    }

    public function setContentEN($content_en)
    {
        $this->content_en = $content_en;
        return $this;
    }

    public function getCreationDate()
    {
        return $this->creationDate;
    }

    public function setCreationDate($creationDate)
    {
        $this->creationDate = $creationDate;
        return $this;
    }

    public function getWriter()
    {
        return $this->writer;
    }

    public function setWriter($writer)
    {
        $this->writer = $writer;
        return $this;
    }

}