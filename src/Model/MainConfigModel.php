<?php
/**
 * @author Nathan JANCZEWSKI <nathan@janczewski.fr>
 */

namespace App\Model;


use App\Entity\Config;

class MainConfigModel
{

    /** @var Config */
    private $companyName;

    /** @var Config */
    private $siteURL;

    public function setCompanyName(Config $config)
    {
        $this->companyName = $config;
        return $this;
    }

    public function setSiteURL(Config $config)
    {
        $this->siteURL = $config;
        return $this;
    }

    public function getCompanyName()
    {
        return $this->companyName;
    }

    public function getSiteURL() {
        return $this->siteURL;
    }

    public function getConfig() {
        $conf = [];

        foreach ($this as $key => $value) {
            $conf[$key] = $value;
        }

        return $conf;
    }

}