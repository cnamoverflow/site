<?php
/**
 * @author Nathan JANCZEWSKI <nathan@janczewski.fr>
 */

namespace App\Entity\Blocks;

use App\Services\FakePath;
use App\Traits\IDTrait;
use App\Traits\OrderTrait;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\Mapping as ORM;


/**
 * @ORM\Entity
 * @ORM\Table(name="Block")
 * @ORM\InheritanceType("JOINED")
 * @ORM\DiscriminatorColumn(name="type", type="string")
 */
abstract class Block
{

    use IDTrait, OrderTrait;

    public abstract function getBlockType(): string;

    public abstract function getBlockTypeName(): string;

    public abstract function getParameters(FakePath $fp, EntityManagerInterface $emi): array;

}