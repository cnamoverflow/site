<?php
/**
 * @author Nathan JANCZEWSKI <nathan@janczewski.fr>
 */

namespace App\Forms;

use App\Entity\Page;
use Doctrine\ORM\EntityRepository;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\IntegerType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class MainMenuType extends AbstractType
{

    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->add('text_fr', TextType::class)
                ->add('text_en', TextType::class)
                ->add('destination', EntityType::class, [
                    'class' => Page::class,
                    'query_builder' => function (EntityRepository $er) {
                        return $er->createQueryBuilder('p');
                    },
                    'choice_label' => 'title_fr'
                ])
                ->add('order', IntegerType::class)
                ->add('submit', SubmitType::class);

    }

    public function configureOptions(OptionsResolver $resolver)
    {
    }
}