<?php
/**
 * @author Nathan JANCZEWSKI <nathan@janczewski.fr>
 */

namespace App\Forms;


use App\Model\SuperConfigModel;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class SuperConfigType extends AbstractType
{

    public function buildForm(FormBuilderInterface $builder, array $options)
    {

        $builder->add('footer', ConfigBooleanType::class, ['label' => ' ', 'text' => 'Footer enabled ?'])
            ->add('dirpub', ConfigStringType::class, ['label' => 'Directeur de publication'])
            ->add('submit', SubmitType::class, ['label' => 'Sauvegarder']);

    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => SuperConfigModel::class
        ]);
    }

}