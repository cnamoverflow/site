var Encore = require('@symfony/webpack-encore');

Encore
    .setOutputPath('public/assets/')
    .setPublicPath('/assets')

    .addStyleEntry('blocks/bandmenu_css', './assets/css/blocks/bandmenu.scss')
    .addStyleEntry('blocks/news_css', './assets/css/blocks/news.scss')
    .addStyleEntry('blocks/title_css', './assets/css/blocks/title.scss')
    .addStyleEntry('blocks/text_css', './assets/css/blocks/text.scss')

    .addEntry('blocks/bandmenu_js', './assets/js/blocks/bandmenu.js')
    .addEntry('blocks/news_js', './assets/js/blocks/news.js')
    .addEntry('blocks/title_js', './assets/js/blocks/title.js')
    .addEntry('blocks/text_js', './assets/js/blocks/text.js')

    .addEntry('base', './assets/js/base.js')
    .addEntry('adminbase', './assets/js/admin/base.js')

    .addEntry('adminpage', './assets/js/admin/page.js')
    .addEntry('adminnews', './assets/js/admin/news.js')
    .addEntry('adminmainmenu', './assets/js/admin/mainmenu.js')

    .addEntry('page_home', './assets/js/page_home.js')
    .addEntry('user', './assets/js/user.js')

    .enableSingleRuntimeChunk()

    .cleanupOutputBeforeBuild()
    .enableSourceMaps(!Encore.isProduction())
    .enableVersioning(Encore.isProduction())
    .enableSassLoader()
    .enableLessLoader()

    .enableTypeScriptLoader()
    .enableForkedTypeScriptTypesChecking()

    .addLoader({
        test: /\.svg$/,
        loader: 'svg-inline-loader'
    })
    //.autoProvidejQuery()
;

module.exports = Encore.getWebpackConfig();
