<?php
/**
 * @author Nathan JANCZEWSKI <nathan@janczewski.fr>
 */

namespace App\Traits;


trait SlugTrait
{
    /**
     * @ORM\Column(type="string", length=30, unique=true)
     */
    protected $slug;

    public function getSlug(): ?string
    {
        return $this->slug;
    }

    public function setSlug(string $slug)
    {
        $this->slug = $slug;
    }

    public function updateGeneratedSlug(string $title)
    {
        $slug = $title;

        $slug = strtolower($slug);
        $slug = preg_replace("/[^a-zA-Z0-9\/_|+ -]/", '', $slug);
        $slug = preg_replace("/[\/_|+ -]+/", '_', $slug);
        $slug = trim($slug);

        $this->slug = $slug;
    }
}