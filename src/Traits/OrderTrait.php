<?php
/**
 * @author Nathan JANCZEWSKI <nathan@janczewski.fr>
 */

namespace App\Traits;


trait OrderTrait
{
    /**
     * @ORM\Column(type="integer", name="`order`")
     */
    protected $order;

    public function getOrder(): ?int
    {
        return $this->order;
    }

    public function setOrder(int $order)
    {
        $this->order = $order;
    }

}