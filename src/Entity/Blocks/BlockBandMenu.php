<?php
/**
 * @author Nathan JANCZEWSKI <nathan@janczewski.fr>
 */

namespace App\Entity\Blocks;


use App\Entity\PseudoEntity\BandMenu;
use App\Services\FakePath;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity()
 */
class BlockBandMenu extends Block
{

    /**
     * @ORM\OneToOne(targetEntity="App\Entity\PseudoEntity\BandMenu", inversedBy="block", cascade={"persist", "remove"})
     */
    private $bandMenu;

    public function getBandMenu(): BandMenu
    {
        return $this->bandMenu;
    }

    public function setBandMenu(BandMenu $bandMenu): BlockBandMenu
    {
        $this->bandMenu = $bandMenu;
        return $this;
    }

    public function getBlockType(): string
    {
        return BlockType::BandMenu;
    }

    public function getBlockTypeName(): string
    {
        return "Menu en bande";
    }

    public function getParameters(FakePath $fp, EntityManagerInterface $emi): array
    {
        return [
            'menu' => $this->bandMenu
        ];
    }

}