import Quill from 'quill';

export let DEFAULT_GETTER = (e) => {
    return e.value;
};

export let CREATE_QUILL_INITIALIZER = (elt_class, toolbar_class) => {
    return (element) => {
        let elt = element.getElementsByClassName(elt_class)[0];

        let quill = new Quill(elt, {
            modules: {
                toolbar: element.getElementsByClassName(toolbar_class)[0]
            },
            theme: 'snow'
        });

        quill.setContents(JSON.parse(elt.innerText));

        return quill;
    }
};

export let QUILL_GETTER = (component, helper) => {
    return JSON.stringify(helper.getContents());
};

export default class EditorComponent {

    constructor(titre) {
        this.titre     = titre;
        this.component = {};
        this.helpers   = {};
        this.key       = "";
    }

    setComponent(key, element) {
        let elt       = document.createElement('div');
        elt.innerHTML = element.trim();
        elt           = elt.firstChild;
        elt.setAttribute("data-content", key);
        this.component = elt;
        this.key       = key;

        this.helpers = {};

        return this;
    }

    initialize(init = null) {
        this.helpers = init(this.component);
        return this;
    }

    setValueGetter(callback) {
        this.valueGetter = callback;
        return this;
    }

    getValue() {
        return this.valueGetter(this.component, this.helpers);
    }

    toElement() {
        let container = document.createElement('div');

        let titre       = document.createElement('div');
        titre.innerText = this.titre;

        container.appendChild(titre);
        container.appendChild(this.component);

        return container;
    }

}