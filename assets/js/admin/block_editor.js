import '../../css/quillbox.scss';
import EditorComponent, { CREATE_QUILL_INITIALIZER, DEFAULT_GETTER, QUILL_GETTER } from "./EditorComponent";

function buildFormTitle(data) {
    return [
        (new EditorComponent("Ordre")).setComponent('ordre', `<input name="ordre" value="${data["order"]}" required/>`).setValueGetter(DEFAULT_GETTER),
        (new EditorComponent("Titre")).setComponent('content', `<input name="content" value="${data['content'] !== null ? data['content'] : ''}" required />`).setValueGetter(DEFAULT_GETTER)
    ];
}

function buildFormText(data) {
    return [
        (new EditorComponent("Ordre")).setComponent('ordre', `<input name="ordre" value="${data["order"]}" required/>`).setValueGetter(DEFAULT_GETTER),
        (new EditorComponent("Text")).setComponent('content', `
            <div class="quillbox-editor">
                <div class="quillbox quill-toolbar">
                    <span class="ql-formats">
                      <select class="ql-font"></select>
                      <select class="ql-size"></select>
                    </span>
                    <span class="ql-formats">
                      <button class="ql-bold"></button>
                      <button class="ql-italic"></button>
                      <button class="ql-underline"></button>
                      <button class="ql-strike"></button>
                    </span>
                    <span class="ql-formats">
                      <select class="ql-color"></select>
                      <select class="ql-background"></select>
                    </span>
                    <span class="ql-formats">
                      <button class="ql-script" value="sub"></button>
                      <button class="ql-script" value="super"></button>
                    </span>
                    <span class="ql-formats">
                      <button class="ql-header" value="1"></button>
                      <button class="ql-header" value="2"></button>
                      <button class="ql-blockquote"></button>
                    </span>
                    <span class="ql-formats">
                      <button class="ql-list" value="ordered"></button>
                      <button class="ql-list" value="bullet"></button>
                      <button class="ql-indent" value="-1"></button>
                      <button class="ql-indent" value="+1"></button>
                    </span>
                    <span class="ql-formats">
                      <button class="ql-direction" value="rtl"></button>
                      <select class="ql-align"></select>
                    </span>
                    <span class="ql-formats">
                      <button class="ql-link"></button>
                      <button class="ql-image"></button>
                      <button class="ql-video"></button>
                      <button class="ql-formula"></button>
                    </span>
                    <span class="ql-formats">
                      <button class="ql-clean"></button>
                    </span>
                </div>
                <div class="quill-editor">${data['content']}</div>
            </div>
            `).initialize(CREATE_QUILL_INITIALIZER("quill-editor", "quill-toolbar")).setValueGetter(QUILL_GETTER)
    ];
}

/**
 * @param blocktype Type of the block
 * @param data      Actual content of the block
 * @param callback  Function to call when the user has finished editing the data
 */
export function showBlockEditor(data, callback) {
    let editor = document.getElementById("block_editor_content");

    let bt     = document.getElementById("block_editor_close");
    bt.onclick = function () {
        document.getElementById("block_editor").classList.remove("slide_in");
    };

    let input        = [];
    editor.innerHTML = "";

    switch (data["type"]) {
        case "Texte":
            input = buildFormText(data);
            break;
        case "Titre":
            input = buildFormTitle(data);
            break;
        default:
            break;
    }

    for (let elt in input) {
        editor.appendChild(input[elt].toElement());
    }

    document.getElementById("block_editor").classList.add("slide_in");


    bt         = document.getElementById("block_editor_save");
    bt.onclick = function () {
        let data = {};

        for (let elt in input) {
            elt = input[elt];
            data[elt.key] = elt.getValue();
        }

        console.log(data);

        callback(data);
    };
}
