<?php
/**
 * @author Nathan JANCZEWSKI <nathan@janczewski.fr>
 */

namespace App\Services;


use App\Entity\PseudoEntity\FooterItem;
use Doctrine\Common\Persistence\ObjectManager;

class FooterFetcherService
{

    private $om;

    public function __construct(ObjectManager $om)
    {
        $this->om = $om;
    }

    public function getFooter(): array
    {
        return $this->om->getRepository(FooterItem::class)->findBy([], ["order" => "ASC"]);
    }

}