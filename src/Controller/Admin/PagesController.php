<?php
/**
 * @author Nathan JANCZEWSKI <nathan@janczewski.fr>
 */

namespace App\Controller\Admin;

use App\Entity\Blocks\Block;
use App\Entity\Blocks\BlockType;
use App\Entity\Page;
use App\Forms\PageType;
use App\Serialization\ErrorResponse;
use App\Services\BlockBuilderService;
use Doctrine\ORM\EntityManagerInterface;
use Exception;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class PagesController extends AbstractController
{

    /** @var EntityManagerInterface */
    private $emi;

    /** @var BlockBuilderService */
    private $bbs;

    public function __construct(EntityManagerInterface $emi, BlockBuilderService $bbs)
    {
        $this->emi = $emi;
        $this->bbs = $bbs;
    }

    /**
     * @Route("/admin/page", name="admin_page_list")
     */
    public function page_editor_list()
    {
        $params = [];

        if (!$this->isGranted('ROLE_SUPER_ADMIN'))
            $params['superadminOnly'] = false;

        $pages = $this->getDoctrine()->getRepository(Page::class)->findBy($params, ['title_fr' => 'ASC']);
        return $this->render('admin/pages/list.html.twig', ['pages' => $pages]);
    }

    /**
     * @Route("/admin/page/new", name="admin_page_new")
     */
    public function page_editor_new(Request $request)
    {

        $page = new Page();
        $form = $this->createForm(PageType::class, $page);

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $page->setCreationDate(new \DateTime());
            $page->updateGeneratedSlug($page->getTitleEn());

            $doctrine = $this->getDoctrine()->getManager();

            $doctrine->persist($page);
            $doctrine->flush();

            return $this->redirectToRoute('admin_page_edit', ['id' => $page->getID()]);
        }

        return $this->render('admin/pages/add.html.twig', ['form' => $form->createView()]);
    }

    /**
     * @Route("/admin/page/edit/{id}", name="admin_page_edit")
     */
    public function page_editor_edit(Request $request, string $id)
    {

        $params = ['id' => $id];

        if (!$this->isGranted('ROLE_SUPER_ADMIN'))
            $params['superadminOnly'] = false;

        $page = $this->getDoctrine()->getRepository(Page::class)->findOneBy($params);

        if ($page === null) {
            return $this->redirectToRoute('admin_page_list');
        }

        $form = $this->createForm(PageType::class, $page);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid())
        {
            $page = $form->getData();
            $page->setCreationDate(new \DateTime());
            $page->updateGeneratedSlug($page->getTitleEn());

            $doctrine = $this->getDoctrine()->getManager();

            $doctrine->persist($page);
            $doctrine->flush();
        }

        return $this->render('admin/pages/edit.html.twig', ["entity" => $page, "form" => $form->createView()]);
    }









    /************************************
     *          API LIKE ROUTES         *
     ************************************/

    /**
     * @Route("/admin/page/delete/{id}", name="admin_page_delete")
     */
    public function delete(Request $rq, string $id)
    {
        $page = $this->getDoctrine()->getRepository(Page::class)->findOneBy(['id' => $id]);

        if ($page !== null) {
            $this->getDoctrine()->getManager()->remove($page);
            $this->getDoctrine()->getManager()->flush();
            return new Response("Page deleted!", Response::HTTP_GONE);
        } else if (!$this->isGranted('ROLE_SUPER_ADMIN') && $page->isSuperadminOnly()) {
            return new Response("You don't have permission to do so", Response::HTTP_FORBIDDEN);
        } else {
            return new Response("Page not found!", Response::HTTP_NOT_FOUND);
        }
    }

    /**
     * @Route("/admin/page/{id}/block/{blocktype}", name="admin_page_block_add", methods={"POST"})
     */
    public function addBlock(Request $rq, string $id, string $blocktype)
    {
        /** @var Page $page */
        $page = $this->emi->getRepository(Page::class)->findOneBy(['id' => $id]);

        if ($page === null)
            return new ErrorResponse('This page does not exists!');

        if (!$this->isGranted('ROLE_SUPER_ADMIN') && $page->isSuperadminOnly()) {
            return new Response("You don't have permission to do so", Response::HTTP_FORBIDDEN);
        }

        $block = $this->bbs->buildBlock($blocktype, count($page->getBlocks()));

        if ($block === null) {
            return new ErrorResponse('This block type does not exists!');
        }

        $page->addBlock($block);

        $this->emi->persist($block);
        $this->emi->persist($page);
        $this->emi->flush();

        return new JsonResponse(json_encode(['id' => $block->getID()]), 201);
    }

    /**
     * @TODO: refactor this to prevent title changement, this is block editing, not page editing
     * @Route("/admin/page/{idPage}/block/{idBlock}", name="admin_page_block_edit", methods={"PUT"})
     */
    public function editBlock(Request $rq, string $idPage, string $idBlock)
    {
        if ($rq->getContentType() !== 'json' || !$rq->getContent())
            return new ErrorResponse('You did not send JSON content!');

        /** @var Page $page */
        $page = $this->getDoctrine()->getRepository(Page::class)->findOneBy(['id' => $idPage]);

        if (!$this->isGranted('ROLE_SUPER_ADMIN') && $page->isSuperadminOnly()) {
            return new Response("You don't have permission to do so", Response::HTTP_FORBIDDEN);
        }

        if ($page !== null) {
            $data = json_decode($rq->getContent(), true);

            try {
                $id = $this->bbs->editBlock($idBlock, $data);
            } catch (Exception $e) {
                return new ErrorResponse($e->getMessage());
            }

            if (array_key_exists('title_fr', $data))
                $page->setTitleFr($data[ 'title_fr' ]);

            if (array_key_exists('title_en', $data))
                $page->setTitleFr($data[ 'title_en' ]);


            $doc = $this->getDoctrine()->getManager();

            $doc->persist($page);
            $doc->flush();

            $data = [
                'id' => $id
            ];

            return new JsonResponse($data);

        }

        return new ErrorResponse('Page not found', 404);
    }

    /**
     * @Route("/admin/page/{id}/block/{blockid}", name="admin_page_block_remove", methods={"DELETE"})
     */
    public function deleteBlock(Request $request, string $id, $blockid)
    {
        /** @var Page $page */
        $page = $this->getDoctrine()->getRepository(Page::class)->findOneBy(['id' => $id]);

        if ($page === null) {
            return new ErrorResponse('Page not found!', 404);
        }

        if (!$this->isGranted('ROLE_SUPER_ADMIN') && $page->isSuperadminOnly()) {
            return new Response("You don't have permission to do so", Response::HTTP_FORBIDDEN);
        }

        $block = $this->getDoctrine()->getRepository(Block::class)->findOneBy(['id' => $blockid]);

        if ($block === null) {
            return new ErrorResponse('Block not found!', 404);
        }

        $doc = $this->getDoctrine()->getManager();

        if ($page->getBlocks()->contains($block)) {
            $page->getBlocks()->removeElement($block);
            $doc->persist($page);
        } else {
            return new ErrorResponse('Block is not on this page!', 400);
        }

        $doc->remove($block);
        $doc->flush();

        return new ErrorResponse('Removed!', 410);
    }

    /**
     * @Route("/admin/page/{id}/block/{blockid}", name="admin_page_block_get", methods={"GET"})
     */
    public function getBlock(Request $rq, string $id, string $blockid)
    {
        /** @var Block $block */
        $block = $this->getDoctrine()->getRepository(Block::class)->findOneBy(['id' => $blockid]);

        if ($block === null)
            return new ErrorResponse('This block does not exists!');

        /** @var Page $page */
        $page = $this->getDoctrine()->getRepository(Page::class)->findOneBy(['id' => $id]);

        if ($page !== null) {
            if (!$this->isGranted('ROLE_SUPER_ADMIN') && $page->isSuperadminOnly()) {
                return new Response("You don't have permission to do so", Response::HTTP_FORBIDDEN);
            }

            // BlockType::Text
            // BlockType::Title
            $data = [
                'id' => $block->getID(),
                'type' => $block->getBlockTypeName(),
                'order' => $block->getOrder()
            ];

            switch ($block->getBlockType()) {
                case BlockType::Text:
                    $data[ 'content' ] = $block->getText();
                    break;
                case BlockType::Title:
                    $data[ 'content' ] = $block->getTitle();
                    break;
                default:
                    break;
            }

            return new JsonResponse($data);
        }

        return new ErrorResponse('Page not found', 404);
    }

}