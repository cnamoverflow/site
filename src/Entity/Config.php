<?php
/**
 * @author Nathan JANCZEWSKI <nathan@janczewski.fr>
 */

namespace App\Entity;
use App\Traits\IDTrait;
use ArrayAccess;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity
 * @ORM\Table(name="Config")
 */
class Config
{
    use IDTrait;

    /**
     * @ORM\Column(type="string", length=30, unique=true)
     */
    protected $key;

    /**
     * @ORM\Column(type="string", length=30, nullable=true)
     */
    protected $value;

    public function __construct(string $key = null, string $value = null)
    {
        $this->key = $key;
        $this->value = $value;
    }

    public function getKey()
    {
        return $this->key;
    }

    public function setKey(string $key): Config
    {
        $this->key = $key;
        return $this;
    }

    public function getValue()
    {
        return $this->value;
    }

    public function setValue(?string $value): Config
    {
        $this->value = $value;
        return $this;
    }

}